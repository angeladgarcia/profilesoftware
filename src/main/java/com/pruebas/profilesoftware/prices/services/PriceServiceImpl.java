package com.pruebas.profilesoftware.prices.services;

import com.pruebas.profilesoftware.exceptions.GlobalException;
import com.pruebas.profilesoftware.prices.dao.Price;
import com.pruebas.profilesoftware.prices.dto.PriceDto;
import com.pruebas.profilesoftware.prices.repository.IPriceRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Service
public class PriceServiceImpl implements IPriceService {
    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd-HH.mm");
    private final IPriceRepository iPriceRepository;
    private final ModelMapper modelMapper;

    public PriceServiceImpl(IPriceRepository iPriceRepository, ModelMapper modelMapper) {
        this.iPriceRepository = iPriceRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<PriceDto> findPricePvp(String priceDate, Long productId, Long brandId) throws GlobalException {
        validateInputData(priceDate, productId, brandId);
        LocalDateTime date = LocalDateTime.parse(priceDate, DATE_TIME_FORMATTER);
        List<Price> prices = iPriceRepository.findByStartDateBeforeAndEndDateAfterAndProductIdAndBrandId(date, date, productId, brandId)
                .orElseThrow(() -> new GlobalException("No prices found"));

        if (prices.isEmpty()) {
            throw new GlobalException("No prices found");
        }

        Price pvpPrice = prices.stream()
                .max(Comparator.comparing(Price::getPriority))
                .orElseThrow(() -> new GlobalException("No prices found"));

        return Optional.of(modelMapper.map(pvpPrice, PriceDto.class));
    }

    private void validateInputData(String priceDateStr, Long productId, Long brandId) throws GlobalException {
        List<Price> pricesByBrand = iPriceRepository.findAllByBrandId(brandId).orElse(null);
        if (pricesByBrand == null || pricesByBrand.isEmpty()) {
            throw new GlobalException("The brandId does not exist.");
        }

        List<Price> pricesByProduct = iPriceRepository.findAllByProductId(productId).orElse(null);
        if (pricesByProduct == null || pricesByProduct.isEmpty()) {
            throw new GlobalException("The productId does not exist.");
        }

        try {
            LocalDateTime.parse(priceDateStr, DATE_TIME_FORMATTER);
        } catch (Exception e) {
            throw new GlobalException("Invalid input date format, please use yyyy-MM-dd-HH.mm format");
        }
    }
}
