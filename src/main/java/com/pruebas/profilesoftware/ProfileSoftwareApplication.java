package com.pruebas.profilesoftware;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProfileSoftwareApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProfileSoftwareApplication.class, args);
	}

}
