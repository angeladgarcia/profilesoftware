package com.pruebas.profilesoftware.prices.services;

import com.pruebas.profilesoftware.exceptions.GlobalException;
import com.pruebas.profilesoftware.prices.dto.PriceDto;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class PriceServiceTest {

    @Autowired
    private IPriceService priceService;

    @BeforeEach
    void setUp() {}

    @AfterEach
    void tearDown() {}

    @Test
    void findPricePvp() throws GlobalException {
        String priceDate = "2020-06-14-10.00";
        Long productId = 35455L;
        Long brandId = 1L;
        var pricePvp = priceService.findPricePvp(priceDate, productId, brandId);
        assertTrue(pricePvp.isPresent(), "Price is not found.");

        var expectedPrice = new BigDecimal("35.50");
        assertEquals(expectedPrice, pricePvp.map(PriceDto::getPrice).orElse(BigDecimal.ZERO));
    }
}