package com.pruebas.profilesoftware;

import com.pruebas.profilesoftware.prices.services.IPriceService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class ProfileSoftwareApplicationTests {

	@Autowired
	private IPriceService priceService;

	@Test
	void contextLoads() {
		Assertions.assertNotNull(priceService);
	}
}
