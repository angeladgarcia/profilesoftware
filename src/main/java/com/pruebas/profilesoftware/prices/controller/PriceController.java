package com.pruebas.profilesoftware.prices.controller;

import com.pruebas.profilesoftware.exceptions.GlobalException;
import com.pruebas.profilesoftware.prices.dto.PriceDto;
import com.pruebas.profilesoftware.prices.services.IPriceService;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@OpenAPIDefinition
@RequestMapping("/price")
public class PriceController {

    private final IPriceService iPriceService;

    public PriceController(IPriceService iPriceService) {
        this.iPriceService = iPriceService;
    }

    @GetMapping("/get-price-pvp/date/{date}/product/{idProduct}/brand/{idBrand}")
    public ResponseEntity<PriceDto> getPricePvp(@Parameter(in = ParameterIn.PATH) @DateTimeFormat(pattern = "yyyy-MM-dd-HH.mm") @PathVariable("date") String date,
                                                @Parameter(in = ParameterIn.PATH) @PathVariable("idProduct") Long idProduct,
                                                @Parameter(in = ParameterIn.PATH) @PathVariable("idBrand") Long idBrand) throws GlobalException {
        // Validate if parameters are null
        if (date == null || idProduct == null || idBrand == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        Optional<PriceDto> optionalPrice = iPriceService.findPricePvp(date, idProduct, idBrand);
        if (optionalPrice.isPresent()) {
            PriceDto price = optionalPrice.get();
            return new ResponseEntity<>(price, HttpStatus.OK);
        } else {
            // Manejar el caso en el que el Optional está vacío
            return new ResponseEntity<>(new PriceDto(), HttpStatus.NOT_FOUND);
        }

    }
}
