package com.pruebas.profilesoftware.prices.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.pruebas.profilesoftware.prices.dao.Brand;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class PriceDto implements Serializable {
    @JsonIgnore
    private Long id;

    @JsonIgnore
    private Brand brand;

    @Size(min = 1)
    @NotNull
    private Long brandId;

    @JsonProperty("unpackNested")
    private void unpackNested() {
        this.brandId = this.brand.getId();
    }

    @NotNull
    private LocalDateTime startDate;
    @NotNull
    private LocalDateTime endDate;
    @JsonIgnore
    @NotNull
    private Integer priceList;
    @Size(min = 1)
    @NotNull
    private Long productId;
    @NotNull
    @JsonIgnore
    private Integer priority;
    @NotNull
    @JsonProperty("" + "price")
    private BigDecimal price;
    @JsonIgnore
    @Size(max = 5)
    @NotNull
    private String curr;
}