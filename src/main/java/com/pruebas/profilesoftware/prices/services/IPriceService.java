package com.pruebas.profilesoftware.prices.services;

import com.pruebas.profilesoftware.exceptions.GlobalException;
import com.pruebas.profilesoftware.prices.dto.PriceDto;

import java.util.Optional;

public interface IPriceService {
    Optional<PriceDto> findPricePvp(String priceDate, Long productId, Long brandId) throws GlobalException;
}
