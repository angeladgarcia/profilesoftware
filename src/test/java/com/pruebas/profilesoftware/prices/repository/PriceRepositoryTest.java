package com.pruebas.profilesoftware.prices.repository;

import com.pruebas.profilesoftware.prices.dao.Price;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class PriceRepositoryTest {
    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd-HH.mm");
    private static final Long PRODUCT_ID = 35455L;
    public static final Long BRAND_ID = 1L;
    @Autowired
    private IPriceRepository priceRepository;

    @Test
    void findByParams() {
        String datePriceNow = "2020-06-14-10.00";
        LocalDateTime priceDate = LocalDateTime.parse(datePriceNow, DATE_TIME_FORMATTER);

        // Test 1: petición a las 10:00 del día 14 del producto 35455 para la brand 1 (ZARA)
        Optional<List<Price>> priceByParams = priceRepository.findByStartDateBeforeAndEndDateAfterAndProductIdAndBrandId(priceDate, priceDate, PRODUCT_ID, BRAND_ID);
        assertEquals(1, priceByParams.map(List::size).orElse(0));

        // Test 2: petición a las 16:00 del día 14 del producto 35455 para la brand 1 (ZARA)
        datePriceNow = "2020-06-14-16.00";
        priceDate = LocalDateTime.parse(datePriceNow, DATE_TIME_FORMATTER);
        Optional<List<Price>> priceByParams2 = priceRepository.findByStartDateBeforeAndEndDateAfterAndProductIdAndBrandId(priceDate, priceDate, PRODUCT_ID, BRAND_ID);
        assertEquals(2, priceByParams2.map(List::size).orElse(0));

        // Test 3: petición a las 21:00 del día 14 del producto 35455 para la brand 1 (ZARA)
        datePriceNow = "2020-06-14-21.00";
        priceDate = LocalDateTime.parse(datePriceNow, DATE_TIME_FORMATTER);
        Optional<List<Price>> priceByParams3 = priceRepository.findByStartDateBeforeAndEndDateAfterAndProductIdAndBrandId(priceDate, priceDate, PRODUCT_ID, BRAND_ID);
        assertEquals(1, priceByParams3.map(List::size).orElse(0));

        // Test 4: petición a las 10:00 del día 15 del producto 35455 para la brand 1 (ZARA)
        datePriceNow = "2020-06-15-10.00";
        priceDate = LocalDateTime.parse(datePriceNow, DATE_TIME_FORMATTER);
        Optional<List<Price>> priceByParams4 = priceRepository.findByStartDateBeforeAndEndDateAfterAndProductIdAndBrandId(priceDate, priceDate, PRODUCT_ID, BRAND_ID);
        assertEquals(2, priceByParams4.map(List::size).orElse(0));

        // Test 5: petición a las 21:00 del día 16 del producto 35455 para la brand 1 (ZARA)
        datePriceNow = "2020-06-16-21.00";
        priceDate = LocalDateTime.parse(datePriceNow, DATE_TIME_FORMATTER);
        Optional<List<Price>> priceByParams5 = priceRepository.findByStartDateBeforeAndEndDateAfterAndProductIdAndBrandId(priceDate, priceDate, PRODUCT_ID, BRAND_ID);
        assertEquals(2, priceByParams5.map(List::size).orElse(0));
    }
}