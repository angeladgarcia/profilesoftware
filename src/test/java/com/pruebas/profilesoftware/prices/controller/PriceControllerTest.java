package com.pruebas.profilesoftware.prices.controller;

import com.pruebas.profilesoftware.exceptions.GlobalException;
import org.hamcrest.core.Is;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.util.stream.Stream;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
class PriceControllerTest {

    public static final String PRODUCT_ID = "35455";
    public static final String BRAND_ID = "1";
    public static final String BASE_URL = "/price/get-price-pvp/date/";

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    WebApplicationContext webApplicationContext;

    /*
        2020-06-14-10.00 con el producto 35455 para la brand 1 (ZARA)
        2020-06-14-16.00 con el producto 35455 para la brand 1 (ZARA)
        2020-06-14-21.00 con el producto 35455 para la brand 1 (ZARA)
        2020-06-15-10.00 con el producto 35455 para la brand 1 (ZARA)
        2020-06-16-21.00 con el producto 35455 para la brand 1 (ZARA)
    */
    @Test
    void findPricePVP() throws GlobalException {
        Stream.of(
                new TestData("2020-06-14-10.00", 35.50),
                new TestData("2020-06-14-16.00", 25.45),
                new TestData("2020-06-14-21.00", 35.50),
                new TestData("2020-06-15-10.00", 30.50),
                new TestData("2020-06-16-21.00", 38.95)
        ).forEach(testData -> {
            try {
                mockMvc.perform(get(BASE_URL + testData.date() + "/product/" + PRODUCT_ID + "/brand/" + BRAND_ID)
                                .contentType(MediaType.APPLICATION_JSON))
                        .andExpect(status().isOk())
                        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                        .andExpect(jsonPath("$.price", Is.is(testData.price())));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }
    record TestData(String date, double price) {}
}
