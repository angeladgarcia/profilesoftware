package com.pruebas.profilesoftware.exceptions;

public class GlobalException extends RuntimeException {
    public GlobalException(String message) {
        super(message);
    }
}