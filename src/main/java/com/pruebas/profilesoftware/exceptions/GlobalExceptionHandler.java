package com.pruebas.profilesoftware.exceptions;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(GlobalException.class)
    public ResponseEntity<String> handleStudentDeleteException(GlobalException ex) {
        return ResponseEntity.badRequest().body(ex.getMessage());
    }
}
